<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('insert','StudentController@insertform');
Route::post('create','StudentController@insert');
Route::get('view','StudentController@view');

// edit view
// Route::get('edit/{id}','StudentController@edit');
Route::get('edit/{something}','StudentController@edit', ['middleware' => 'role:editor', function ($id) {
    return "Only big boys/girls can see this.";
}]);
// edit post
Route::post('edit/{id}','StudentController@update');
// delete post
Route::get('delete/{id}','StudentController@delete');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('post', 'PostController');
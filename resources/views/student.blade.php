<html>

   <head>
      <title>Student Management | Add</title>
       <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
   </head>
   
   <body>
      <div class="container">
          <?php
          if(session('status')){ ?>
              <div class="alert alert-success">
                  <?php echo session('status'); ?>
              </div>
          <?php }?>
          <ul class="list-group list-group-horizontal">
             <li class="list-group-item"><a href="{{url('insert')}}">Click here</a> to Insert</li>
             <li class="list-group-item"><a href="{{url('view')}}">Click here</a> to View</li>
          </ul>
          <?php
          if(Request::segment(1) != 'view'){ ?>
          <?php
             if(isset($users)){
                $url  =  "edit/".$users->id;
             }else{
                $url  =  "create";
             }
          ?>

          <form action = "/<?php echo $url;?>" method = "post" class="">
             <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
             <table class="table table-striped">
                <tr>
                   <td>Name</td>
                   <td><input type='text' class="form-control" name='stud_name' value="{{ $users->name ?? NULL }}" /></td>
                </tr>
                <tr>
                   <td colspan = '2'>
                      <input type = 'submit' class="btn btn-default" value = "<?php if(Request::segment(1) == 'insert'){?>Add student <?php }else{ echo 'Update User'; }?>"/>
                   </td>
                </tr>
             </table>
          </form>
          <?php }?>
          <?php if(Request::segment(1) != 'insert' && Request::segment(1) != 'edit'){?>
          <table class="table table-striped">
             <tr>
                <td>ID</td>
                <td>Name</td>
                <td>Action</td>
             </tr>
             <?php foreach ($users as $user) {?>
             <tr>
                <td><?php echo $user->id ?></td>
                <td><?php echo $user->name ?></td>
                <td><a href="edit/<?php echo $user->id;?>" class="btn btn-info">Edit</a> || <a href="delete/<?php echo $user->id;?>" class="btn btn-danger">Delete</a></td>
             </tr>
             <?php } ?>
             <tr>
                <td colspan="3">{{ $users->links("pagination::bootstrap-4") }}</td>
             </tr>
          </table>
          <?php }?>
      </div>
   </body>
</html>
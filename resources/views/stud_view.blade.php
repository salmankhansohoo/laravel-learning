<html>
   
   <head>
      <title>View Student Records</title>
      <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
   </head>
   
   <body>
      @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
      @endif
      <table class="table table-striped">
         <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Action</td>
         </tr>
         @foreach ($users as $user)
         <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->name }}</td>
            <td><a href="edit/{{$user->id}}">Edit</a> || <a href="delete/{{$user->id}}">Delete</a></td>
         </tr>
         @endforeach
      </table>

       
   </body>
</html>
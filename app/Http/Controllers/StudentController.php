<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Student;
use App\Http\Controllers\Controller;

class StudentController extends Controller
{
    public function insertform(){
		return view('student');
	}

	public function insert(Request $request){
		
		$name 	=	$request->input('stud_name');
		
		$obj = new Student;
		$obj->name = $name;
		$obj->save();

		return redirect('view')->with('status', 'User Inserted!');
	}

	public function view(){
		$users	=	DB::table('student')->paginate(5);
		return view('student', ['users'=>$users]);
	}

	public function edit($id){
		// $users	=	DB::table('student')->where('id', $id)->get();
		$users	=	Student::find($id);

		return view('student', compact('users'));
	}

	public function update(Request $request, $id) {

		$name 	=	$request->input('stud_name');
		// $users 	=	DB::table('student')->where('id', $id)->update(['name'=> $name]);
		$users 	=	Student::find($id);
		$users->name = $request->stud_name;
		$users->save();
		
		return redirect('view')->with('status', 'User Updated!');
   }

   public function delete($id){
   		
   		$users 	=	Student::find($id);
   		$users->delete();
   		// DB::table('student')->where('id', $id)->delete();
   		return redirect('view')->with('status', 'User Deleted!');
   }
}
